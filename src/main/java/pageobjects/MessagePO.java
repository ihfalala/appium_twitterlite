package pageobjects;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
public class MessagePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public MessagePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="//*[@class='android.widget.EditText']")
    private WebElement messageEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Send'])")
    private WebElement sendButton;

    /**
     * masukan text pesan
     */
    public void enterMessage(String messageText){

        appium.enterText(messageEditText, messageText, true);
    }

    /**
     * klik send untuk mengirim pesan
     */
    public void clickSend(){ sendButton.click(); }

}
