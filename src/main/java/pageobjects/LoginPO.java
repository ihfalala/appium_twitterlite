package pageobjects;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class LoginPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public LoginPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[1]")
    private WebElement usnEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[2]")
    private WebElement passEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Log in'])")
    private WebElement loginBUtton2;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='The username and password you entered did not match our records. Please double-check and try again.'])")
    private WebElement errortxt;

    /**
     *memasukan username
     */
    public void enterUsername(String username){

        appium.enterText(usnEditText, username, true);
    }

    /**
     *memasukan password
     */
    public void enterPassword(String password){

        appium.enterText(passEditText, password, true);
    }

    /**
     * Click login untuk masuk ke branda
     */
    public void clickLogin2(){ loginBUtton2.click(); }

    /**
     * display message error login
     */
    public boolean messageError() {
        return appium.isElementDisplayed(errortxt);
    }
}
