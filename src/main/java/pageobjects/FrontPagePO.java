package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class FrontPagePO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public FrontPagePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Not now'])")
    private WebElement notNowBUtton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Log in'])[1]")
    private WebElement loginBUtton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Sign up'])[1]")
    private WebElement SignupBUtton;

    /**
     * Click not now for twitter on the app
     */
    public void clickNotnow(){ notNowBUtton.click(); }

    /**
     * Click login untuk melakukan login
     */
    public void clickLogin(){ loginBUtton.click(); }

    /**
     * Click signup untuk regist
     */
    public void clickSignup(){ SignupBUtton.click(); }
}
