package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class NavigationPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public NavigationPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='Profile'])")
    private WebElement ProfileButton;

    /**
     * klik profile untuk melihat profile anda
     */
    public void clickProfile(){ ProfileButton.click(); }

}
