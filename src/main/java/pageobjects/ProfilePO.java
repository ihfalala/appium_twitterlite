package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class ProfilePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ProfilePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Edit profile'])")
    private WebElement EditProfileButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Follow'])")
    private WebElement followButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Following'])")
    private WebElement followingButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Unfollow'])")
    private WebElement unfollowButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Message'])")
    private WebElement dmButton;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='caca'])")
    private WebElement verifyEditProfile;

    /**
     * klik edit profile untuk mengedit
     */
    public void clickEditProfile(){ EditProfileButton.click(); }

    /**
     * klik follow
     */
    public void clickFollow(){ followButton.click(); }

    /**
     * klik following for unfollow
     */
    public void clickFollowing(){ followingButton.click(); }

    /**
     * klik unfollow
     */
    public void clickUnfollow(){ unfollowButton.click(); }

    /**
     * verify follow people
     */
    public boolean verifyFollow() { return appium.isElementDisplayed(followingButton); }

    /**
     * verify unfollow people
     */
    public boolean verifyUnfollow() { return appium.isElementDisplayed(followButton); }

    /**
     * klik message untuk mengirim pesan
     */
    public void clickDm(){
        dmButton.click();
    }

    /**
     * verify edit profile
     */
    public boolean verifEditProfile() { return appium.isElementDisplayed(verifyEditProfile); }

}
