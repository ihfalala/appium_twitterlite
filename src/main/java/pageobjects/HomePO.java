package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class HomePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public HomePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='Home'])[2]")
    private WebElement homeLogin;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='Search and explore'])")
    private WebElement searchButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Spinner'])[1]")
    private WebElement profileMenu;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Compose a Tweet'])")
    private WebElement tweetButton;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='hallo'])")
    private WebElement tweetText;

    /**
     * klik search untuk mengarahkan ke halaman search dan explore
     */
    public void clickSearch(){ searchButton.click(); }

    /**
     * verify login in home
     */
    public boolean verifyHomeLogin() { return appium.isElementDisplayed(homeLogin); }

    /**
     * klik profile menu untuk menampilkan navigation bar
     */
    public void clickProfileMenu(){
        profileMenu.click();
    }

    /**
     * klik untuk mmebuat tweet
     */
    public void clickTweet(){
        tweetButton.click();
    }

    /**
     * klik tweet untuk mlihat detail tweet
     */
    public void clickTweetText(){
        tweetText.click();
    }

    /**
     * verify delete
     * @return
     */
    public boolean verifyDelete() { return appium.isElementDisplayed(tweetText);
    }

    /**
     * verify send Tweet
     * @return
     */
    public boolean verifyDSendTweet() { return appium.isElementDisplayed(tweetText);
    }
}
