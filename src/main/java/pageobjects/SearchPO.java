package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class SearchPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public SearchPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="//*[@class='android.widget.EditText']")
    private WebElement searchEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='#COVID20'])")
    private WebElement suggestButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='lala @peoniyy'])")
    private WebElement suggestButton2;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='Photos'])")
    private WebElement photosButton;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='Videos'])")
    private WebElement videosButton;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='Image'])")
    private WebElement verifyPhoto;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='video'])")
    private WebElement verifyVideo;

    @AndroidFindBy(xpath="(//*[@class='android.view.View'][@text='#COVID20'])")
    private WebElement verifySearch;

    /**
     *masukan teks dan klik suggestion
     */
    public void enterText(String text)throws InterruptedException{
        searchEditText.click();
        appium.enterText(searchEditText, text, true);
    }

    /**
     * klik suggestion untuk memilih yang mana akan disearch
     */
    public void clickSuggest(){
        suggestButton.click();

    }

    /**
     * klik suggestion untuk memilih profile
     */
    public void clickSuggestProfile(){ suggestButton2.click(); }

    /**
     * klik photos untuk memfilter berdasarkan photo
     */
    public void clickPhoto(){ photosButton.click(); }

    /**
     * klik videos untuk memfilter berdasarkan video
     */
    public void clickVideo(){
        videosButton.click();

    }

    /**
     * verify photo
     */
    public boolean verifyPhoto() {
        return appium.isElementDisplayed(verifyPhoto);
    }

    /**
     * verify video
     */
    public boolean verifyVideo() {
        return appium.isElementDisplayed(verifyVideo);
    }

    /**
     * verify search
     * @return
     */
    public String verifySearch(){ verifySearch.getText();
        return null;
    }
}
