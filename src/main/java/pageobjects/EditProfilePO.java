package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class EditProfilePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public EditProfilePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[1]")
    private WebElement nameProfileEdit;

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[2]")
    private WebElement bioEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[3]")
    private WebElement lokasiEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[4]")
    private WebElement websiteEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Save'])")
    private WebElement saveButton;

    /**
     *edit nama
     */
    public void enterNameEdit(String namaEdit){

        appium.enterText(nameProfileEdit, namaEdit, true);
    }

    /**
     *edit bio
     */
    public void enterBio(String bio){

        appium.enterText(bioEditText, bio, true);
    }

    /**
     *edit lokasi
     */
    public void enterLokasi(String lokasi){

        appium.enterText(lokasiEditText, lokasi, true);
    }

    /**
     *edit website
     */
    public void enterWebsite(String website){

        appium.enterText(websiteEditText, website, true);
    }

    /**
     * klik save
     */
    public void clickSaveEdit(){ saveButton.click();
    }
}
