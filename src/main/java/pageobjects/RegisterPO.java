package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class RegisterPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RegisterPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[1]")
    private WebElement nameEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.EditText'])[2]")
    private WebElement noHandphoneEditText;

    @AndroidFindBy(xpath="//*[@class='android.widget.Spinner']")
    private WebElement birthdateText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'])[3]")
    private WebElement setBirthday;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Next'])")
    private WebElement nextButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Sign up'])")
    private WebElement SignupBUtton2;

    @AndroidFindBy(id="(//*[@class='android.view.View'])[31]")
    private WebElement errorMessage;

    /**
     *masukan nama
     */
    public void enterName(String username){

        appium.enterText(nameEditText, username, true);
    }

    /**
     *masukan password
     */
    public void enterNoHandphone(String password){

        appium.enterText(noHandphoneEditText, password, true);
    }

    /**
     *masukan tanggal lahir
     */
    public void clickBirthday(){ birthdateText.click(); }

    /**
     * Click set pada tanggal lahir
     */
    public void clickSetBirth(){ setBirthday.click(); }

    /**
     * Click next untuk melanjutkan signup
     */
    public void clickNext(){ nextButton.click(); }

    /**
     * Click sign up
     */
    public void clickSignup2(){ SignupBUtton2.click(); }

    /**
     * display message error regist
     */
    public boolean regisErrorTextIsDisplayed() {
        return appium.isElementDisplayed(errorMessage);
    }
}
