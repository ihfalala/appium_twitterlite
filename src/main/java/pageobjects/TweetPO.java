package pageobjects;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class TweetPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public TweetPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="//*[@class='android.widget.EditText']")
    private WebElement tweetEditText;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Tweet'])")
    private WebElement sendTweetButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Spinner'][@text='More'])")
    private WebElement settingTweetButton;

    @AndroidFindBy(xpath="(//*[@class='android.view.MenuItem'][@text='Delete'])")
    private WebElement deleteButton;

    @AndroidFindBy(xpath="(//*[@class='android.widget.Button'][@text='Delete'])")
    private WebElement deleteTweetButton;

    /**
     *memasukan tweet
     */
    public void enterTweet(String tweetText){

        appium.enterText(tweetEditText, tweetText, true);
    }

    /**
     * klik send tweet
     */
    public void clickSendTweet(){ sendTweetButton.click(); }

    /**
     * klik setting untuk delete tweet
     */
    public void clickSettingTweet(){ settingTweetButton.click(); }

    /**
     * klik delete
     */
    public void clickDelete(){ deleteButton.click(); }

    /**
     * klik delete tweet
     */
    public void clickDeleteTweet(){ deleteTweetButton.click(); }
}
