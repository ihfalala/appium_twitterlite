package steps;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.*;
import utilities.ThreadManager;

public class EditProfileSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private HomePO home = new HomePO(driver);
    private NavigationPO navigation = new NavigationPO(driver);
    private ProfilePO profile = new ProfilePO(driver);
    private EditProfilePO editProfile = new EditProfilePO(driver);

    @And("user klik profile foto")
    public void user_click_profile_foto() {
        home.clickProfileMenu();
    }

    @And("user klik profile")
    public void user_click_profile() {
        navigation.clickProfile();
    }

    @And("user klik edit profile")
    public void user_click_edit_profile() {
        profile.clickEditProfile();
    }

    @And("user edit nama {string}")
    public void user_edit_nama(String namaEdit){
        editProfile.enterNameEdit(namaEdit);
    }

    @And("user edit bio {string}")
    public void user_edit_bio(String bio){
        editProfile.enterBio(bio);
    }

    @And("user edit lokasi {string}")
    public void user_edit_lokasi(String lokasi){
        editProfile.enterLokasi(lokasi);
    }

    @And("user edit website {string}")
    public void user_edit_website(String website){
        editProfile.enterWebsite(website);
    }

    @And("user klik save")
    public void user_click_save() {
        editProfile.clickSaveEdit();
    }

    @Then("verify edit profile")
    public boolean verify_edit_profile() {
        return profile.verifEditProfile();
    }
}
