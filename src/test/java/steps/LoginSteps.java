package steps;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.*;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

public class LoginSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private FrontPagePO frontPage = new FrontPagePO(driver);
    private LoginPO login = new LoginPO(driver);
    private HomePO home = new HomePO(driver);
    private  AppiumHelpers appium = new AppiumHelpers(driver);

//appium.setContext("WEBVIEW_chrome");

    @When("user klik not now")
    public void user_click_not_now() {
        frontPage.clickNotnow();
    }

    @And("user klik login untuk masuk ke halaman login")
    public void user_click_login() {
        frontPage.clickLogin();
    }

    @And("user masukan username {string}")
    public void user_masukan_username(String username){
        login.enterUsername(username);
    }

    @And("user masukan password {string}")
    public void user_masukan_password(String password){
        login.enterPassword(password);
    }
    @And("user klik login untuk masuk ke beranda")
    public void user_click_login2() {
        login.clickLogin2();
    }

    @Then("verify home login")
    public boolean verify_home_login(){
        return home.verifyHomeLogin();
    }

    @Then("display login message error")
    public boolean display_message_error(){
        return login.messageError();
    }
}
