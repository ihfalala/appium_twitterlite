package steps;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.*;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

public class SearchSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private HomePO home = new HomePO(driver);
    private SearchPO search = new SearchPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);

    @And("user klik search untuk menuju ke halaman search")
    public void user_click_search() {
        home.clickSearch();
    }

    @And("user masukan text {string}")
    public void user_input_teks_and_enter(String text)throws InterruptedException {
        search.enterText(text);
    }

    @And("user pilih suggestion")
    public void user_choose_suggestion() {
        search.clickSuggest();
        appium.scrollUp(3, 2);
    }

    @Then("verify text by keyword {string}")
    public void verify_text_by_keyword(String keyword) {
        for(int i= 1; i<6; i++) {
            String element = driver.findElement(By.xpath("(//*[@class='android.view.View'][@text='#COVID20'])["+i+"]")).getText();
            System.out.println(element);
            Assert.assertTrue(element.toLowerCase().contains(keyword.toLowerCase()));
        }
    }
}
