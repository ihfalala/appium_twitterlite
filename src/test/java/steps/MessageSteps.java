package steps;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.*;
import utilities.ThreadManager;
public class MessageSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ProfilePO profile = new ProfilePO(driver);
    private MessagePO message = new MessagePO(driver);

    @And("user klik direct message")
    public void user_click_direct_message() {
        profile.clickDm();
    }

    @And("user masukan pesan {string}")
    public void user_input_message(String messageText) {
        message.enterMessage(messageText);
    }

    @And("user klik send")
    public void user_click_send() {
        message.clickSend();
    }

    @Then("verify message by keyword {string}")
    public void verify_text_by_keyword(String keyword) {
        String element = driver.findElement(By.xpath("(//*[@class='android.view.View'][@text='hallo'])")).getText();
        System.out.println(element);
        Assert.assertTrue(element.toLowerCase().contains(keyword.toLowerCase()));
    }

}
