package steps;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import pageobjects.*;
import utilities.ThreadManager;

public class FollowSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private SearchPO search = new SearchPO(driver);
    private ProfilePO profile = new ProfilePO(driver);

    @And("user klik profile suggestion")
    public void user_click_profile_suggestion() {
        search.clickSuggestProfile();
    }

    @And("user klik follow")
    public void user_click_follow() {
        profile.clickFollow();
    }

    @Then("verify follow")
    public boolean verify_follow(){
        return profile.verifyFollow();
    }

    @And("user klik following untuk mengunfollow")
    public void user_click_following() { profile.clickFollowing();
    }

    @And("user klik unfollow")
    public void user_click_unfollow() { profile.clickUnfollow();
    }

    @Then("verify unfollow")
    public boolean verify_unfollow(){
        return profile.verifyUnfollow();
    }

}
