package steps;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.*;
import utilities.ThreadManager;

public class FilterSearchSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private SearchPO search = new SearchPO(driver);

    @And("user klik photos")
    public void user_click_photos() {
        search.clickPhoto();
    }

    @And("user klik videos")
    public void user_click_videos() { search.clickVideo(); }

    @Then("verify filter video")
    public boolean verify_filter_video(){
        return search.verifyVideo();
    }

    @Then("verify filter photo")
    public boolean verify_filter_photo(){
        return search.verifyPhoto();
    }
}
