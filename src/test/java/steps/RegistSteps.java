package steps;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import pageobjects.*;
import utilities.ThreadManager;

public class RegistSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private FrontPagePO frontPage = new FrontPagePO(driver);
    private RegisterPO regist = new RegisterPO(driver);

    @When("user klik signup untuk melakukan registrasi")
    public void user_click_signup() {
        frontPage.clickSignup();
    }

    @And("user masukan name {string}")
    public void user_masukan_name(String name){
        regist.enterName(name);
    }

    @And("user masukan no handphone {string}")
    public void user_masukan_nomor(String noHP){
        regist.enterNoHandphone(noHP);
    }

    @And("user klik tanggal lahir")
    public void user_click_tanggal_lahir() {
        regist.clickBirthday();
    }

    @And("user set tanggal lahir")
    public void user_set_tanggal_lahir() {
        regist.clickSetBirth();
    }

    @And("user klik next")
    public void user_click_next() {
        regist.clickNext();
    }

    @And("user klik signup")
    public void user_click_signup2() {
        regist.clickSignup2();
    }

    @Then("menampilkan regist message error")
    public boolean system_display_regist_error() {
        return regist.regisErrorTextIsDisplayed();
    }

}
