package steps;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.*;
import utilities.ThreadManager;
public class TweetSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private HomePO home = new HomePO(driver);
    private TweetPO tweet = new TweetPO(driver);

    @And("user klik compose a tweet")
    public void user_click_compose_tweet() {
        home.clickTweet();
    }

    @And("user masukan tweet {string}")
    public void user_input_tweet(String tweetText){
        tweet.enterTweet(tweetText);
    }

    @And("user klik send tweet")
    public void user_click_send_tweet() {
        tweet.clickSendTweet();
    }

    @And("user klik tweet")
    public void user_click_tweet() {
        home.clickTweetText();
    }

    @And("user klik setting tweet")
    public void user_click_setting_tweet() {
        tweet.clickSettingTweet();
    }

    @And("user klik delete")
    public void user_click_delete() {
        tweet.clickDelete();
    }

    @And("user klik delete tweet")
    public void user_click_delete_tweet() {
        tweet.clickDeleteTweet();
    }

    @Then("verify delete tweet")
    public boolean verify_delete_tweet() {
        return home.verifyDelete();
    }

    @Then("verify send tweet")
    public boolean verify_send_tweet() {
        return home.verifyDSendTweet();
    }

}
