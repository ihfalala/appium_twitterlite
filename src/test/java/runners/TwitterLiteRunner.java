package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/TwitterLite/cucumber-report.json",  "html:target/results/TwitterLite"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@twitter"}
)

public class TwitterLiteRunner extends BaseTestRunnerAndroid{
}
