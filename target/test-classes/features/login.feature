@login

Feature: login
  Scenario: success
    When user klik not now
    And user klik login untuk masuk ke halaman login
    And user masukan username 'cipululupopo'
    And user masukan password '2desember1998'
    And user klik login untuk masuk ke beranda
    Then verify home login

  Scenario Outline: username dan password kosong
    When user klik not now
    And user klik login untuk masuk ke halaman login
    And user masukan username <username>
    And user masukan password <password>
    Then user klik login untuk masuk ke beranda
    Examples:
      | username       | password        |
      | ''             | '2desember1998' |
      | 'cipululupopo' | ''              |

  Scenario Outline: username dan password tidak valid
    When user klik not now
    And user klik login untuk masuk ke halaman login
    And user masukan username <username>
    And user masukan password <password>
    And user klik login untuk masuk ke beranda
    Then display login message error
    Examples:
      | username       | password        |
      | 'cipululupopo' | 'xxxx'          |
      | 'xxxx'         | '2desember1998' |
