@regist

Feature: regist

  Scenario Outline: name dan password tidak diisi
    When user klik not now
    And user klik signup untuk melakukan registrasi
    And user masukan name <name>
    And user masukan no handphone <noHP>
    And user klik tanggal lahir
    And user set tanggal lahir
    Then user klik next
    Examples:
      | name   | noHP          |
      | ''     | '08128890807' |
      | 'lala' | ''            |

  Scenario: tanggal lahir tidak diisi
    When user klik not now
    And user klik signup untuk melakukan registrasi
    And user masukan name 'lala'
    And user masukan no handphone '08128890807'
    Then user klik next

  Scenario: tanggal lahir tidak valid
    When user klik not now
    And user klik signup untuk melakukan registrasi
    And user masukan name 'lala'
    And user masukan no handphone '08128890807'
    And user klik tanggal lahir
    And user set tanggal lahir
    And user klik next
    And user klik next
    And user klik signup
    Then menampilkan message error