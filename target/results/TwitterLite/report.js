$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/tweet.feature");
formatter.feature({
  "name": "tweet",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@tweet"
    }
  ]
});
formatter.scenario({
  "name": "membuat tweet",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@tweet"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user klik not now",
  "keyword": "When "
});
formatter.match({
  "location": "steps.LoginSteps.user_click_not_now()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik login untuk masuk ke halaman login",
  "keyword": "And "
});
formatter.match({
  "location": "steps.LoginSteps.user_click_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user masukan username \u0027cipululupopo\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "steps.LoginSteps.user_masukan_username(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user masukan password \u00272desember1998\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "steps.LoginSteps.user_masukan_password(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik login untuk masuk ke beranda",
  "keyword": "And "
});
formatter.match({
  "location": "steps.LoginSteps.user_click_login2()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik compose a tweet",
  "keyword": "And "
});
formatter.match({
  "location": "steps.TweetSteps.user_click_compose_tweet()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user masukan tweet \u0027hallo\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "steps.TweetSteps.user_input_tweet(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik send tweet",
  "keyword": "And "
});
formatter.match({
  "location": "steps.TweetSteps.user_click_send_tweet()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify send tweet",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.TweetSteps.verify_send_tweet()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik tweet",
  "keyword": "And "
});
formatter.match({
  "location": "steps.TweetSteps.user_click_tweet()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik setting tweet",
  "keyword": "And "
});
formatter.match({
  "location": "steps.TweetSteps.user_click_setting_tweet()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik delete",
  "keyword": "And "
});
formatter.match({
  "location": "steps.TweetSteps.user_click_delete()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik delete tweet",
  "keyword": "And "
});
formatter.match({
  "location": "steps.TweetSteps.user_click_delete_tweet()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify delete tweet",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.TweetSteps.verify_delete_tweet()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});